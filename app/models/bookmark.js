var mongoose = require('mongoose');

var bookmarkSchema = mongoose.Schema({
  book: { type: String, default: '' },
  page: { type: String, default: '' },
  lastUpdated: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('Bookmark', bookmarkSchema);
